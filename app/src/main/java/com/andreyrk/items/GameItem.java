package com.andreyrk.items;

import java.io.File;
import java.nio.file.Paths;

public class GameItem {
    public String fileUri;
    public String fileName;
    public String fileExt;

    public Descriptor descriptor = Descriptor.Item;

    public enum Descriptor {
        Item,
        Item_Recent,
        Item_Favorite
    }

    public GameItem (File file) {
        this.fileUri = file.getAbsolutePath();

        int extIndex = file.getName().lastIndexOf('.');

        this.fileName = file.getName().substring(0, extIndex);
        this.fileExt = file.getName().substring(extIndex + 1);
    }
}
