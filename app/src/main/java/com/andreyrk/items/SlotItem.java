package com.andreyrk.items;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.File;
import java.io.FileReader;

public class SlotItem {

    public SaveInfo saveInfo = new SaveInfo();

    public File saveFile;
    public File thumbnailFile;
    public File infoFile;

    public Bitmap thumbnail;

    public SlotItem(String gamePath, int number) {
        saveInfo.number = number;

        Load(gamePath);
    }

    public void Load(String gamePath) {
        File game = new File(gamePath);

        String parentDir = game.getParent();
        File dir = new File(parentDir, "savefiles");
        dir.mkdir();

        try {
            String gameName = game.getName().replaceFirst("[.][^.]+$", "");

            String numberStr = String.valueOf(saveInfo.number);

            saveFile = new File(dir, gameName + "_" + numberStr + ".sav");
            thumbnailFile = new File(dir, gameName + "_" + numberStr + ".png");
            infoFile = new File(dir, gameName + "_" + numberStr + ".json");

            if (infoFile.exists()) {
                saveInfo = new Gson().fromJson(new JsonReader(new FileReader(infoFile)), SaveInfo.class);
                saveInfo.number = Integer.valueOf(numberStr);
            }

            if (thumbnailFile.exists()) {
                thumbnail = BitmapFactory.decodeFile(thumbnailFile.getAbsolutePath());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class SaveInfo {
        public int number;

        public String date = "";
        public String time = "";
    }
}
