package com.andreyrk.items;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LibraryItem {
    public String title;
    public String fileName;

    public int imageResId;

    public List<String> validExtensions;

    public LibraryItem(String title, String fileName, int imageResId, String... validExtensions) {
        this.title = title;
        this.fileName = fileName;
        this.imageResId = imageResId;
        this.validExtensions = Arrays.asList(validExtensions);
    }
}
