package com.andreyrk.items;

public class SettingsItem {
    public static final int ID_GENERAL = 0;
    public static final int ID_VIDEO = 1;
    public static final int ID_AUDIO = 2;
    public static final int ID_INPUT = 3;

    public String title;
    public String description;
    public int id;
    public int imageResId;

    public Descriptor descriptor = Descriptor.Item;

    public enum Descriptor {
        Item,
        Item_General,
        Item_Video,
        Item_Audio,
        Item_Input
    }

    public SettingsItem(String title, String description, int id, int imageResId, Descriptor descriptor) {
        this.title = title;
        this.description = description;
        this.id = id;
        this.imageResId = imageResId;
        this.descriptor = descriptor;
    }
}
