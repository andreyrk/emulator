package com.andreyrk.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.andreyrk.opengba.R;

public class PauseActivity extends AppCompatActivity {

    public static final int EXIT_CODE = 0;
    public static final int RETURN_CODE = 1;
    public static final int LOAD_CODE = 2;
    public static final int SAVE_CODE = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pause_menu);

        LinearLayout pause_exit = findViewById(R.id.pause_exit);
        pause_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(EXIT_CODE);
                finish();
            }
        });

        LinearLayout pause_return = findViewById(R.id.pause_return);
        pause_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RETURN_CODE);
                finish();
            }
        });

        LinearLayout pause_load = findViewById(R.id.pause_load);
        pause_load.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), SlotChooseActivity.class);
                intent.putExtra("SLOT_MODE", "LOAD");
                startActivityForResult(intent, 0);
            }
        });

        LinearLayout pause_save = findViewById(R.id.pause_save);
        pause_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), SlotChooseActivity.class);
                intent.putExtra("SLOT_MODE", "SAVE");
                startActivityForResult(intent, 0);
            }
        });
    }

    @Override
    public void onBackPressed() {
        setResult(RETURN_CODE);
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case SlotChooseActivity.EXIT_CODE:
                setResult(RETURN_CODE);
                finish();
                break;
            case SlotChooseActivity.RETURN_CODE:
                break;
        }
    }
}
