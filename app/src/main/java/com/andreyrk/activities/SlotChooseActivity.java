package com.andreyrk.activities;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.andreyrk.adapters.LibrarySlotAdapter;
import com.andreyrk.items.SlotItem;
import com.andreyrk.opengba.R;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.google.gson.Gson;

import org.apache.commons.io.FileUtils;

import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class SlotChooseActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private LibrarySlotAdapter adapter;

    public static final int EXIT_CODE = 0;
    public static final int RETURN_CODE = 1;

    List<SlotItem> slots = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slot_choose);

        final String mode = getIntent().getStringExtra("SLOT_MODE");

        TextView label = findViewById(R.id.slot_label);
        label.setText(mode.equals("SAVE") ? getString(R.string.slot_choose_save_label) : getString(R.string.slot_choose_load_label));

        String gamePath = GameActivity.INSTANCE.gamePath;

        for (int i = 1; i <= 10; i++) {
            slots.add(new SlotItem(gamePath, i));
        }

        adapter = new LibrarySlotAdapter(this, slots);
        adapter.setClickListener(new LibrarySlotAdapter.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                SlotItem slot = adapter.getItem(position);

                switch (mode) {
                    case "SAVE":
                        GameActivity.INSTANCE.libretro.saveState(slot.saveFile);

                        try (FileOutputStream out = new FileOutputStream(slot.thumbnailFile, false)) {
                            GameActivity.INSTANCE.libretro.screenBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        slot.saveInfo.date = new SimpleDateFormat(((SimpleDateFormat)DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault())).toLocalizedPattern(), Locale.getDefault()).format(new Date());
                        slot.saveInfo.time = new SimpleDateFormat(((SimpleDateFormat)DateFormat.getTimeInstance(DateFormat.SHORT, Locale.getDefault())).toLocalizedPattern(), Locale.getDefault()).format(new Date());

                        try {
                            FileUtils.writeStringToFile(slot.infoFile, new Gson().toJson(slot.saveInfo), "UTF-8");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        System.out.println("Game state saved to: " + slot.saveFile.getName());

                        setResult(EXIT_CODE);
                        finish();
                        break;
                    case "LOAD":
                        GameActivity.INSTANCE.libretro.loadState(slot.saveFile);
                        System.out.println("Game state loaded from: " + slot.saveFile.getName());

                        setResult(EXIT_CODE);
                        finish();
                        break;
                }
            }
        });
        recyclerView = findViewById(R.id.slot_recycler_view);
        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(this);
        layoutManager.setJustifyContent(JustifyContent.SPACE_AROUND);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }


    @Override
    public void onBackPressed() {
        setResult(RETURN_CODE);
        super.onBackPressed();
    }
}
