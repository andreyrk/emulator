package com.andreyrk.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RadioGroup;

import com.andreyrk.opengba.R;
import com.andreyrk.settings.EmulatorSettings;

public class VideoSettingsActivity extends AppCompatActivity {

    private RadioGroup filtering_radioGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_settings);

        filtering_radioGroup = findViewById(R.id.filtering_radioGroup);

        if (EmulatorSettings.INSTANCE.customizationSettings.filterBilinear) {
            filtering_radioGroup.check(R.id.filtering_radioButton_smooth);
        } else {
            filtering_radioGroup.check(R.id.filtering_radioButton_sharp);
        }

        filtering_radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.filtering_radioButton_smooth:
                        EmulatorSettings.INSTANCE.customizationSettings.filterBilinear = true;
                        break;
                    case R.id.filtering_radioButton_sharp:
                        EmulatorSettings.INSTANCE.customizationSettings.filterBilinear = false;
                        break;
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        EmulatorSettings.Save();

        super.onDestroy();
    }
}
