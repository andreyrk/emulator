package com.andreyrk.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.andreyrk.adapters.LibraryGameAdapter;
import com.andreyrk.settings.EmulatorSettings;
import com.andreyrk.items.GameItem;
import com.andreyrk.opengba.R;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class LibraryChooseActivity extends AppCompatActivity {

    private SearchView searchView;
    private TextView label;

    private RecyclerView recyclerView;
    private LibraryGameAdapter adapter;
    private ProgressBar progressBar;
    private TextView warning;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_library_choose);

        searchView = findViewById(R.id.games_search_view);
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setIconified(false);
            }
        });

        String coreTitle = getIntent().getStringExtra("CORE_TITLE");

        label = findViewById(R.id.games_label);
        label.setText(coreTitle);

        progressBar = findViewById(R.id.games_progress_bar);
        warning = findViewById(R.id.games_warning);

        EmulatorSettings.INSTANCE.gameSettings.games = new ArrayList<>();

        adapter = new LibraryGameAdapter(this, EmulatorSettings.INSTANCE.gameSettings.games);
        recyclerView = findViewById(R.id.games_recycler_view);
        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(this);
        layoutManager.setJustifyContent(JustifyContent.SPACE_AROUND);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        new UpdateGames(this).execute();
    }

    public void SearchGames(File file, List<GameItem> gameList, List<String> validExtensions) {
        File[] files = file.listFiles();

        if (files == null) return;

        for (File f : files) {
            if (f.isDirectory()) {
                SearchGames(f, gameList, validExtensions);
            }

            if (f.isFile()) {
                for (String ext : validExtensions) {
                    if (f.getPath().endsWith("." + ext)) {
                        gameList.add(new GameItem(f));
                        break;
                    }
                }
            }
        }
    }

    private class UpdateGames extends AsyncTask<Void, Void, Void> {

        LibraryChooseActivity activity;

        public UpdateGames(LibraryChooseActivity activity) {
            super();

            this.activity = activity;

            activity.warning.setVisibility(View.GONE);
            activity.recyclerView.setVisibility(View.GONE);
            activity.progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            EmulatorSettings.INSTANCE.gameSettings.games = new ArrayList<>();

            ArrayList<String> validExtensions = activity.getIntent().getStringArrayListExtra("VALID_EXTENSIONS");
            SearchGames(Environment.getExternalStorageDirectory(), EmulatorSettings.INSTANCE.gameSettings.games, validExtensions);

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            activity.adapter = new LibraryGameAdapter(activity, EmulatorSettings.INSTANCE.gameSettings.games);
            activity.adapter.setClickListener(new LibraryGameAdapter.ItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    GameItem item = adapter.getItem(position);

                    Intent intent = new Intent(getBaseContext(), GameActivity.class);
                    intent.putExtra("CORE_PATH", getIntent().getStringExtra("CORE_PATH"));
                    intent.putExtra("GAME_PATH", item.fileUri);
                    startActivity(intent);
                }
            });
            activity.recyclerView.setAdapter(adapter);

            if (EmulatorSettings.INSTANCE.gameSettings.games.size() == 0) {
                activity.warning.setVisibility(View.VISIBLE);
            } else {
                activity.recyclerView.setVisibility(View.VISIBLE);
            }

            activity.progressBar.setVisibility(View.GONE);

        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }
}
