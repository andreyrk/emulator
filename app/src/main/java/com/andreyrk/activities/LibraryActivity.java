package com.andreyrk.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;

import com.andreyrk.adapters.LibraryItemAdapter;
import com.andreyrk.adapters.LibrarySettingsAdapter;
import com.andreyrk.items.LibraryItem;
import com.andreyrk.items.SettingsItem;
import com.andreyrk.settings.EmulatorSettings;
import com.andreyrk.opengba.R;

import java.util.ArrayList;

public class LibraryActivity extends AppCompatActivity {

    private SearchView searchView;

    private RecyclerView libraryRecyclerView;
    private LibraryItemAdapter libraryAdapter;

    private RecyclerView settingsRecyclerView;
    private LibrarySettingsAdapter settingsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_library);

        new EmulatorSettings(this);
        EmulatorSettings.INSTANCE.setup();

        searchView = findViewById(R.id.library_search_view);
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setIconified(false);
            }
        });

        libraryAdapter = new LibraryItemAdapter(this, EmulatorSettings.INSTANCE.librarySettings.items);
        libraryRecyclerView = findViewById(R.id.library_recycler_view);
        libraryRecyclerView.setNestedScrollingEnabled(false);
        libraryRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        libraryRecyclerView.setAdapter(libraryAdapter);

        libraryAdapter.setClickListener(new LibraryItemAdapter.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                LibraryItem item = libraryAdapter.getItem(position);

                Intent intent = new Intent(getBaseContext(), LibraryChooseActivity.class);
                intent.putExtra("CORE_TITLE", item.title);
                intent.putExtra("CORE_PATH", item.fileName);
                intent.putExtra("VALID_EXTENSIONS", new ArrayList<>(item.validExtensions));
                startActivity(intent);
            }
        });


        settingsAdapter = new LibrarySettingsAdapter(this, EmulatorSettings.INSTANCE.customizationSettings.settings);
        settingsRecyclerView = findViewById(R.id.settings_recycler_view);
        settingsRecyclerView.setNestedScrollingEnabled(false);
        settingsRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        settingsRecyclerView.setAdapter(settingsAdapter);

        settingsAdapter.setClickListener(new LibrarySettingsAdapter.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                SettingsItem item = settingsAdapter.getItem(position);

                switch (item.id) {
                    case SettingsItem.ID_GENERAL:
                        break;
                    case SettingsItem.ID_VIDEO:
                        Intent intent = new Intent(getBaseContext(), VideoSettingsActivity.class);
                        startActivity(intent);
                        break;
                    case SettingsItem.ID_AUDIO:
                        break;
                    case SettingsItem.ID_INPUT:
                        break;
                }
            }
        });
    }
}
