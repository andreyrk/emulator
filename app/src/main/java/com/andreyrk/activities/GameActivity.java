package com.andreyrk.activities;

import com.andreyrk.opengba.R;
import com.andreyrk.libretro.*;
import com.andreyrk.settings.EmulatorSettings;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import fr.arnaudguyon.smartgl.opengl.RenderPassSprite;
import fr.arnaudguyon.smartgl.opengl.SmartGLRenderer;
import fr.arnaudguyon.smartgl.opengl.SmartGLView;
import fr.arnaudguyon.smartgl.opengl.SmartGLViewController;
import fr.arnaudguyon.smartgl.opengl.Sprite;
import fr.arnaudguyon.smartgl.opengl.Texture;
import fr.arnaudguyon.smartgl.touch.TouchHelperEvent;

public class GameActivity extends AppCompatActivity implements SmartGLViewController {

    public static GameActivity INSTANCE;

    public Libretro libretro;
    public SmartGLView screenView;

    public Timer timer;

    public List<InputKey> keys = new ArrayList<>();

    public String gamePath;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        INSTANCE = this;

        screenView = findViewById(R.id.screenView);
        screenView.setDefaultRenderer(this);
        screenView.setController(this);

        // Initiate Libretro

        String corePath = getIntent().getStringExtra("CORE_PATH");
        gamePath = getIntent().getStringExtra("GAME_PATH");

        libretro = new Libretro(this, corePath);
        libretro.init();

        loadGame(gamePath);

        // Initiate input keys
        keys.add(new InputKey(LibretroLibrary.retro_key.RETRO_DEVICE_ID_JOYPAD_DOWN, (ImageButton) findViewById(R.id.button_dpad_down)));
        keys.add(new InputKey(LibretroLibrary.retro_key.RETRO_DEVICE_ID_JOYPAD_LEFT, (ImageButton) findViewById(R.id.button_dpad_left)));
        keys.add(new InputKey(LibretroLibrary.retro_key.RETRO_DEVICE_ID_JOYPAD_UP, (ImageButton) findViewById(R.id.button_dpad_up)));
        keys.add(new InputKey(LibretroLibrary.retro_key.RETRO_DEVICE_ID_JOYPAD_RIGHT, (ImageButton) findViewById(R.id.button_dpad_right)));
        keys.add(new InputKey(LibretroLibrary.retro_key.RETRO_DEVICE_ID_JOYPAD_A, (ImageButton) findViewById(R.id.button_a)));
        keys.add(new InputKey(LibretroLibrary.retro_key.RETRO_DEVICE_ID_JOYPAD_X, (ImageButton) findViewById(R.id.button_x)));
        keys.add(new InputKey(LibretroLibrary.retro_key.RETRO_DEVICE_ID_JOYPAD_Y, (ImageButton) findViewById(R.id.button_y)));
        keys.add(new InputKey(LibretroLibrary.retro_key.RETRO_DEVICE_ID_JOYPAD_B, (ImageButton) findViewById(R.id.button_b)));
        keys.add(new InputKey(LibretroLibrary.retro_key.RETRO_DEVICE_ID_JOYPAD_L, (ImageButton) findViewById(R.id.button_l)));
        keys.add(new InputKey(LibretroLibrary.retro_key.RETRO_DEVICE_ID_JOYPAD_R, (ImageButton) findViewById(R.id.button_r)));
        keys.add(new InputKey(LibretroLibrary.retro_key.RETRO_DEVICE_ID_JOYPAD_SELECT, (ImageButton) findViewById(R.id.button_select)));
        keys.add(new InputKey(LibretroLibrary.retro_key.RETRO_DEVICE_ID_JOYPAD_START, (ImageButton) findViewById(R.id.button_start)));

        for (final InputKey key : keys) {
            key.imageButton.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        libretro.keys.put(key.key.getValue(), true);
                    } else if (event.getAction() == MotionEvent.ACTION_UP) {
                        libretro.keys.put(key.key.getValue(), false);
                    }

                    return true;
                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        System.out.println("Unloading core...");
        libretro.deinit();

        super.onDestroy();
    }

    public void loadGame(String path) {
        boolean success = libretro.loadGame(path);

        if (!success) {
            System.out.println("An error occurred while loading the game.");
            return;
        }

        System.out.println("The game was loaded succesfully.");
    }

    @Override
    protected void onPause() {
        System.out.println("Pause");
        if (screenView != null) {
            screenView.onPause();
        }

        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("Resume");

        if (screenView != null) {
            screenView.onResume();
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getBaseContext(), PauseActivity.class);
        startActivityForResult(intent, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case PauseActivity.EXIT_CODE:
                finish();
                break;
            case PauseActivity.RETURN_CODE:
                break;
            case PauseActivity.LOAD_CODE:
                break;
            case PauseActivity.SAVE_CODE:
                break;
        }
    }

    private Texture screenTexture;
    private Sprite screenSprite;
    private SmartGLRenderer renderer;
    private RenderPassSprite renderPassSprite;

    @Override
    public void onPrepareView(SmartGLView smartGLView) {
        System.out.println("Prepare");

        renderer = smartGLView.getSmartGLRenderer();
        renderer.setClearColor(0, 0, 0, 1);
        renderPassSprite = new RenderPassSprite();
        renderer.addRenderPass(renderPassSprite);
    }

    @Override
    public void onReleaseView(SmartGLView smartGLView) {
        System.out.println("Release");

        if (screenTexture != null) {
            screenTexture.release();
            screenTexture = null;
        }
    }

    @Override
    public void onResizeView(SmartGLView smartGLView) {
        System.out.println("Resize");

        if (screenSprite != null) {
            setupRendering(new Vector2Int(libretro.screenWidth, libretro.screenHeight));
        }
    }

    float frameTime = 0f;
    @Override
    public void onTick(SmartGLView smartGLView) {
        if (libretro.isPaused) return;

        frameTime += renderer.getFrameDuration() * libretro.systemAvInfo.timing.fps;
        for (; frameTime >= 1; frameTime--) {
            libretro.run();
        }

        if (libretro.screenBitmap == null) return;

        if (screenTexture == null) {
            setupRendering(new Vector2Int(libretro.screenWidth, libretro.screenHeight));
        } else {
            screenTexture.release();
        }

        screenTexture = new Texture(Bitmap.createScaledBitmap(GameActivity.INSTANCE.libretro.screenBitmap, (int)screenSprite.getWidth(), (int)screenSprite.getHeight(), EmulatorSettings.INSTANCE.customizationSettings.filterBilinear));
        screenSprite.setTexture(screenTexture);
    }

    @Override
    public void onTouchEvent(SmartGLView smartGLView, TouchHelperEvent touchHelperEvent) {

    }

    public void setupRendering(Vector2Int spriteSize) {
        Vector2Int screenSize = new Vector2Int(renderer.getWidth(), renderer.getHeight());
        Vector2Int scaledSize = ResizeKeepAspect(spriteSize, screenSize);

        screenSprite = new Sprite(scaledSize.width, scaledSize.height);
        screenSprite.setPivot(0.5f, 0f);
        screenSprite.setPos(screenSize.width / 2f, 0f);

        renderPassSprite.clearObjects();
        renderPassSprite.addSprite(screenSprite);
    }

    public Vector2Int ResizeKeepAspect(Vector2Int size, Vector2Int boundary)
    {
        float rnd = Math.min(boundary.width / (float)size.width, boundary.height / (float)size.height);
        return new Vector2Int(Math.round(size.width * rnd), Math.round(size.height * rnd));
    }

    class Vector2Int {
        public int width;
        public int height;

        public Vector2Int(int width, int height) {
            this.width = width;
            this.height = height;
        }
    }

    class InputKey {
        public LibretroLibrary.retro_key key;
        public ImageButton imageButton;

        public InputKey(LibretroLibrary.retro_key key, ImageButton imageButton) {
            this.key = key;
            this.imageButton = imageButton;
        }
    }
}
