package com.andreyrk.settings;

import android.app.Activity;

import com.andreyrk.opengba.R;
import com.andreyrk.items.LibraryItem;
import com.andreyrk.items.SettingsItem;
import com.google.gson.Gson;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Arrays;

public class EmulatorSettings {
    public GameSettings gameSettings;
    public CustomizationSettings customizationSettings;
    public LibrarySettings librarySettings;

    public static EmulatorSettings INSTANCE;

    public transient Activity launcher;

    public EmulatorSettings(Activity launcher) {
        INSTANCE = this;

        gameSettings = new GameSettings();
        customizationSettings = new CustomizationSettings();
        librarySettings = new LibrarySettings();

        EmulatorSettings.Load(launcher);
    }

    public static void Save() {
        File file = new File(EmulatorSettings.INSTANCE.launcher.getApplicationContext().getFilesDir(), "settings.json");

        try {
            FileUtils.writeStringToFile(file, new Gson().toJson(EmulatorSettings.INSTANCE), "UTF-8");

            System.out.println("Xemu's settings were saved successfully.");
        } catch (IOException e) {
            System.out.println("An error occurred while saving Xemu's settings.");
            e.printStackTrace();
        }
    }

    public static void Load(Activity launcher) {
        File file = new File(launcher.getApplicationContext().getFilesDir(), "settings.json");

        if (file.exists()) {
            try {
                String data = FileUtils.readFileToString(file, "UTF-8");
                EmulatorSettings.INSTANCE = new Gson().fromJson(data, EmulatorSettings.class);

                System.out.println("Xemu's settings were loaded successfully.");
            } catch (IOException e) {
                System.out.println("An error occurred while loading Xemu's settings.");
                e.printStackTrace();
            }
        }

        EmulatorSettings.INSTANCE.launcher = launcher;
    }

    public void setup() {
        customizationSettings.settings = Arrays.asList(
                new SettingsItem(launcher.getString(R.string.settings_general), launcher.getString(R.string.settings_general_description), SettingsItem.ID_GENERAL, R.drawable.ic_settings_24, SettingsItem.Descriptor.Item_General),
                new SettingsItem(launcher.getString(R.string.settings_video), launcher.getString(R.string.settings_video_description), SettingsItem.ID_VIDEO, R.drawable.ic_videocam_24, SettingsItem.Descriptor.Item_Video),
                new SettingsItem(launcher.getString(R.string.settings_audio), launcher.getString(R.string.settings_audio_description), SettingsItem.ID_AUDIO, R.drawable.ic_audiotrack_24, SettingsItem.Descriptor.Item_Audio),
                new SettingsItem(launcher.getString(R.string.settings_input), launcher.getString(R.string.settings_input_description), SettingsItem.ID_INPUT, R.drawable.ic_gamepad_24, SettingsItem.Descriptor.Item_Input)
        );

        librarySettings.items = Arrays.asList(
                new LibraryItem(launcher.getString(R.string.library_all_games_label), "mgba", R.drawable.ic_core_all_games, "gba", "gbc", "gb")
        );
    }
}
