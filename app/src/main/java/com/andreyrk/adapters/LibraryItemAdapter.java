package com.andreyrk.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.andreyrk.items.LibraryItem;
import com.andreyrk.opengba.R;

import java.util.ArrayList;
import java.util.List;

public class LibraryItemAdapter extends RecyclerView.Adapter<LibraryItemAdapter.ViewHolder> {

    private Context mContext;
    private List<LibraryItem> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    public LibraryItemAdapter(Context context, List<LibraryItem> data) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.library_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LibraryItem item = getItem(position);

        holder.image.setImageDrawable(ContextCompat.getDrawable(mContext, item.imageResId));
        holder.label.setText(item.title);

        List<String> exts = new ArrayList<>();
        for (String ext : item.validExtensions) {
            exts.add("." + ext);
        }

        holder.description.setText(TextUtils.join(", ", exts));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView image;
        public TextView label;
        public TextView description;

        ViewHolder(View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.item_image);
            label = itemView.findViewById(R.id.item_label);
            description = itemView.findViewById(R.id.item_description);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) {
                mClickListener.onItemClick(view, getAdapterPosition());
            }
        }
    }

    public LibraryItem getItem(int id) {
        return mData.get(id);
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}