package com.andreyrk.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.andreyrk.items.SettingsItem;
import com.andreyrk.opengba.R;

import java.util.List;

public class LibrarySettingsAdapter extends RecyclerView.Adapter<LibrarySettingsAdapter.ViewHolder> {

    private Context mContext;
    private List<SettingsItem> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    public LibrarySettingsAdapter(Context context, List<SettingsItem> data) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.settings_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        SettingsItem item = getItem(position);

        holder.image.setImageDrawable(ContextCompat.getDrawable(mContext, item.imageResId));
        holder.label.setText(item.title);
        holder.description.setText(item.description);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView image;
        public TextView label;
        public TextView description;

        ViewHolder(View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.item_image);
            label = itemView.findViewById(R.id.item_label);
            description = itemView.findViewById(R.id.item_description);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) {
                mClickListener.onItemClick(view, getAdapterPosition());
            }
        }
    }

    public SettingsItem getItem(int id) {
        return mData.get(id);
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}