package com.andreyrk.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.andreyrk.items.SlotItem;
import com.andreyrk.opengba.R;

import java.util.List;

public class LibrarySlotAdapter extends RecyclerView.Adapter<LibrarySlotAdapter.ViewHolder> {

    private Context mContext;
    private List<SlotItem> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    public LibrarySlotAdapter(Context context, List<SlotItem> data) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.slot_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        SlotItem item = getItem(position);

        holder.image.setImageBitmap(item.thumbnail);
        holder.label.setText(("Slot " + String.valueOf(item.saveInfo.number)));
        holder.date.setText(item.saveInfo.date);
        holder.time.setText(String.valueOf(item.saveInfo.time));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView image;
        public TextView label;
        public TextView date;
        public TextView time;

        ViewHolder(View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.slot_image);
            label = itemView.findViewById(R.id.slot_label);
            date = itemView.findViewById(R.id.slot_date);
            time = itemView.findViewById(R.id.slot_time);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) {
                mClickListener.onItemClick(view, getAdapterPosition());
            }
        }
    }

    public SlotItem getItem(int id) {
        return mData.get(id);
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}