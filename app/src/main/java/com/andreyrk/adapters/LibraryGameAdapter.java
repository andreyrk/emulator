package com.andreyrk.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.andreyrk.items.GameItem;
import com.andreyrk.opengba.R;

import java.util.List;

public class LibraryGameAdapter extends RecyclerView.Adapter<LibraryGameAdapter.ViewHolder> {

    private Context mContext;
    private List<GameItem> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    public LibraryGameAdapter(Context context, List<GameItem> data) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.game_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        GameItem item = getItem(position);

        holder.image.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_videogame_asset_24));
        holder.label.setText(item.fileName);
        holder.description.setText("." + item.fileExt);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView image;
        public TextView label;
        public TextView description;

        ViewHolder(View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.item_image);
            label = itemView.findViewById(R.id.item_label);
            description = itemView.findViewById(R.id.item_description);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) {
                mClickListener.onItemClick(view, getAdapterPosition());
            }
        }
    }

    public GameItem getItem(int id) {
        return mData.get(id);
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}