package com.andreyrk.libretro;

import com.sun.jna.Callback;
import com.sun.jna.IntegerType;
import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import com.sun.jna.ptr.DoubleByReference;
import com.sun.jna.ptr.FloatByReference;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.ShortByReference;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface LibretroLibrary extends Library {
    int retro_api_version();

    void retro_run();

    void retro_init();
    void retro_deinit();

    byte retro_load_game(retro_game_info gameInfo);
    void retro_unload_game();

    byte retro_serialize(Pointer buffer, SizeT size);
    byte retro_unserialize(Pointer buffer, SizeT size);
    SizeT retro_serialize_size();

    void retro_get_system_info(retro_system_info systemInfo);
    void retro_get_system_av_info(retro_system_av_info systemAvInfo);
    void retro_set_environment(retro_environment_t environment);
    void retro_set_video_refresh(retro_video_refresh_t videoRefresh);
    void retro_set_audio_sample(retro_audio_sample_t audioSample);
    void retro_set_audio_sample_batch(retro_audio_sample_batch_t audioSampleBatch);
    void retro_set_input_poll(retro_input_poll_t inputPoll);
    void retro_set_input_state(retro_input_state_t inputState);

    enum retro_key
    {
        RETRO_DEVICE_ID_JOYPAD_B(0),
        RETRO_DEVICE_ID_JOYPAD_Y(1),
        RETRO_DEVICE_ID_JOYPAD_SELECT(2),
        RETRO_DEVICE_ID_JOYPAD_START(3),
        RETRO_DEVICE_ID_JOYPAD_UP(4),
        RETRO_DEVICE_ID_JOYPAD_DOWN(5),
        RETRO_DEVICE_ID_JOYPAD_LEFT(6),
        RETRO_DEVICE_ID_JOYPAD_RIGHT(7),
        RETRO_DEVICE_ID_JOYPAD_A(8),
        RETRO_DEVICE_ID_JOYPAD_X(9),
        RETRO_DEVICE_ID_JOYPAD_L(10),
        RETRO_DEVICE_ID_JOYPAD_R(11);

        private static final Map<Integer, retro_key> lookup = new HashMap();
        private int value;

        static {
            for(retro_key d : retro_key.values()) {
                lookup.put(d.value, d);
            }
        }

        public static retro_key get(int value) {
            return lookup.get(value);
        }

        public int getValue() {
            return value;
        }

        retro_key(int value) {
            this.value = value;
        }
    };

    enum retro_environment
    {
        RETRO_ENVIRONMENT_EXPERIMENTAL(0x10000),

        RETRO_ENVIRONMENT_SET_PERFORMANCE_LEVEL(8),
        RETRO_ENVIRONMENT_GET_SYSTEM_DIRECTORY(9),
        RETRO_ENVIRONMENT_SET_PIXEL_FORMAT(10),
        RETRO_ENVIRONMENT_SET_INPUT_DESCRIPTORS(11),
        RETRO_ENVIRONMENT_GET_VARIABLE(15),
        RETRO_ENVIRONMENT_SET_VARIABLES(16),
        RETRO_ENVIRONMENT_GET_VARIABLE_UPDATE(17),
        RETRO_ENVIRONMENT_GET_RUMBLE_INTERFACE(23),
        RETRO_ENVIRONMENT_GET_LOG_INTERFACE(27),
        RETRO_ENVIRONMENT_GET_SAVE_DIRECTORY(31),
        RETRO_ENVIRONMENT_SET_SYSTEM_AV_INFO(32),
        RETRO_ENVIRONMENT_SET_CONTROLLER_INFO(35),
        RETRO_ENVIRONMENT_SET_GEOMETRY(37),
        RETRO_ENVIRONMENT_SET_SERIALIZATION_QUIRKS(44),

        RETRO_ENVIRONMENT_SET_MEMORY_MAPS(36 | RETRO_ENVIRONMENT_EXPERIMENTAL.value),
        RETRO_ENVIRONMENT_SET_SUPPORT_ACHIEVEMENTS(42 | RETRO_ENVIRONMENT_EXPERIMENTAL.value),
        RETRO_ENVIRONMENT_GET_AUDIO_VIDEO_ENABLE(47 | RETRO_ENVIRONMENT_EXPERIMENTAL.value);

        private static final Map<Integer, retro_environment> lookup = new HashMap();
        private int value;

        static {
            for(retro_environment d : retro_environment.values()) {
                lookup.put(d.value, d);
            }
        }

        public static retro_environment get(int value) {
            return lookup.get(value);
        }

        public int getValue() {
            return value;
        }

        retro_environment(int value) {
            this.value = value;
        }
    };

    enum retro_pixel_format {
        RETRO_PIXEL_FORMAT_0RGB1555(0),
        RETRO_PIXEL_FORMAT_XRGB8888(1),
        RETRO_PIXEL_FORMAT_RGB565(2),
        RETRO_PIXEL_FORMAT_UNKNOWN(Integer.MAX_VALUE);

        private static final Map<Integer, retro_pixel_format> lookup = new HashMap();
        private int value;

        static {
            for(retro_pixel_format d : retro_pixel_format.values()) {
                lookup.put(d.value, d);
            }
        }

        public static retro_pixel_format get(int value) {
            return lookup.get(value);
        }

        public int getValue() {
            return value;
        }

        retro_pixel_format(int value) {
            this.value = value;
        }
    };

    class retro_system_info extends Structure {
        public String library_name;
        public String library_version;
        public String valid_extensions;
        public byte need_fullpath;
        public byte block_extract;

        @Override
        protected List<String> getFieldOrder() {
            return Arrays.asList("library_name", "library_version", "valid_extensions", "need_fullpath", "block_extract");
        }
    }

    class retro_game_geometry extends Structure {
        public UnsignedInt base_width;
        public UnsignedInt base_height;
        public UnsignedInt max_width;
        public UnsignedInt max_height;

        public float aspect_ratio;

        @Override
        protected List<String> getFieldOrder() {
            return Arrays.asList("base_width", "base_height", "max_width", "max_height", "aspect_ratio");
        }
    };

    class retro_system_timing extends Structure {
        public double fps;
        public double sample_rate;

        @Override
        protected List<String> getFieldOrder() {
            return Arrays.asList("fps", "sample_rate");
        }
    }

    class retro_system_av_info extends Structure {
        public retro_game_geometry geometry;
        public retro_system_timing timing;

        @Override
        protected List<String> getFieldOrder() {
            return Arrays.asList("geometry", "timing");
        }
    }

    class retro_game_info extends Structure {
        public String path;
        public Pointer data;
        public SizeT size;
        public String meta;

        @Override
        protected List<String> getFieldOrder() {
            return Arrays.asList("path", "data", "size", "meta");
        }
    }

    class retro_perf_counter extends Structure {
        public String ident;
        long start;
        long total;
        long call_cnt;
        public byte registered;

        @Override
        protected List<String> getFieldOrder() {
            return Arrays.asList("ident", "start", "total", "call_cnt", "registered");
        }
    }

    interface retro_proc_address_t extends Callback {
        void apply();
    };

    interface retro_get_proc_address_t extends Callback {
        LibretroLibrary.retro_proc_address_t apply(Pointer sym);
    };

    interface retro_log_printf_t extends Callback {
        void apply(int level, Pointer fmt, Object... varargs);
    };

    interface retro_perf_get_time_usec_t extends Callback {
        long apply();
    };

    interface retro_perf_get_counter_t extends Callback {
        long apply();
    };

    interface retro_get_cpu_features_t extends Callback {
        long apply();
    };

    interface retro_perf_log_t extends Callback {
        void apply();
    };

    interface retro_perf_register_t extends Callback {
        void apply(LibretroLibrary.retro_perf_counter counter);
    };

    interface retro_perf_start_t extends Callback {
        void apply(LibretroLibrary.retro_perf_counter counter);
    };

    interface retro_perf_stop_t extends Callback {
        void apply(LibretroLibrary.retro_perf_counter counter);
    };

    interface retro_set_sensor_state_t extends Callback {
        byte apply(int port, int action, int rate);
    };

    interface retro_sensor_get_input_t extends Callback {
        float apply(int port, float id1);
    };

    interface retro_camera_start_t extends Callback {
        byte apply();
    };

    interface retro_camera_stop_t extends Callback {
        void apply();
    };

    interface retro_camera_lifetime_status_t extends Callback {
        void apply();
    };

    interface retro_camera_frame_raw_framebuffer_t extends Callback {
        void apply(IntByReference buffer, int width, int height, SizeT pitch);
    };

    interface retro_camera_frame_opengl_texture_t extends Callback {
        void apply(int texture_id, int texture_target, FloatByReference affine);
    };

    interface retro_location_set_interval_t extends Callback {
        void apply(int interval_ms, int interval_distance);
    };

    interface retro_location_start_t extends Callback {
        byte apply();
    };

    interface retro_location_stop_t extends Callback {
        void apply();
    };

    interface retro_location_get_position_t extends Callback {
        byte apply(DoubleByReference lat, DoubleByReference lon, DoubleByReference horiz_accuracy, DoubleByReference vert_accuracy);
    };

    interface retro_location_lifetime_status_t extends Callback {
        void apply();
    };

    interface retro_set_rumble_state_t extends Callback {
        byte apply(int port, int effect, short strength);
    };

    interface retro_audio_callback_t extends Callback {
        void apply();
    };

    interface retro_audio_set_state_callback_t extends Callback {
        void apply(byte enabled);
    };

    interface retro_frame_time_callback_t extends Callback {
        void apply(long usec);
    };

    interface retro_hw_context_reset_t extends Callback {
        void apply();
    };

    interface uintptr_t extends Callback {
        UintptrTCallback apply(Pointer retro_hw_get_current_framebuffer_t);
    };

    interface retro_hw_get_proc_address_t extends Callback {
        retro_proc_address_t apply(Pointer sym);
    };

    interface retro_keyboard_event_t extends Callback {
        void apply(byte down, int keycode, int character, short key_modifiers);
    };

    interface retro_set_eject_state_t extends Callback {
        byte apply(byte ejected);
    };

    interface retro_get_eject_state_t extends Callback {
        byte apply();
    };

    interface retro_get_image_index_t extends Callback {
        int apply();
    };

    interface retro_set_image_index_t extends Callback {
        byte apply(int index);
    };

    interface retro_get_num_images_t extends Callback {
        int apply();
    };

    interface retro_replace_image_index_t extends Callback {
        byte apply(int index, retro_game_info info);
    };

    interface retro_add_image_index_t extends Callback {
        byte apply();
    };

    interface retro_environment_t extends Callback {
        byte apply(UnsignedInt cmd, Pointer data);
    };

    interface retro_video_refresh_t extends Callback {
        void apply(Pointer data, UnsignedInt width, UnsignedInt height, SizeT pitch);
    };

    interface retro_audio_sample_t extends Callback {
        void apply(short left, short right);
    };

    interface retro_audio_sample_batch_t extends Callback {
        void apply(ShortByReference data, SizeT frames);
    };

    interface retro_input_poll_t extends Callback {
        void apply();
    };

    interface retro_input_state_t extends Callback {
        short apply(UnsignedInt port, UnsignedInt device, UnsignedInt index, UnsignedInt id1);
    };

    interface UintptrTCallback extends Callback {
        int apply();
    };

    class SizeT extends IntegerType {

        public SizeT() {
            this(0);
        }

        public SizeT(long value) {
            super(Native.SIZE_T_SIZE, value, true);
        }
    }

    class UnsignedInt extends IntegerType {

        private static final int SIZE = 4;

        public UnsignedInt() {
            super(SIZE, true);
        }

        public UnsignedInt(long value) {
            super(SIZE, value, true);
        }
    }
}
