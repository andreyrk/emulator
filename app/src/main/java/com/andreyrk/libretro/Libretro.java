package com.andreyrk.libretro;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Environment;

import com.sun.jna.Memory;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.PointerByReference;
import com.sun.jna.ptr.ShortByReference;

import org.apache.commons.io.FileUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;

public class Libretro {
    public LibretroLibrary library;
    public String core;

    public AudioTrack audio;
    public boolean isAudioAvailable = false;
    public boolean isPaused = true;

    public LibretroLibrary.retro_system_info systemInfo;
    public LibretroLibrary.retro_system_av_info systemAvInfo;

    public LibretroLibrary.retro_environment_t retroEnvironmentCallback;
    public LibretroLibrary.retro_video_refresh_t retroVideoRefreshCallback;
    public LibretroLibrary.retro_audio_sample_t retroAudioSampleCallback;
    public LibretroLibrary.retro_audio_sample_batch_t retroAudioSampleBatchCallback;
    public LibretroLibrary.retro_input_poll_t retroInputPollCallback;
    public LibretroLibrary.retro_input_state_t retroInputStateCallback;

    public Map<Integer, Boolean> keys = new HashMap<Integer, Boolean>();;
    public Context context;

    public Libretro(Context context, String core) {
        this.context = context;
        this.core = core;

        library = Native.loadLibrary(core, LibretroLibrary.class);
    }

    public void init() {
        int retroApiVersion = library.retro_api_version();

        System.out.println("-- START RETRO_API_VERSION --");
        System.out.println("RETRO_API_VERSION: " + retroApiVersion);
        System.out.println("-- END RETRO_API_VERSION --");

        systemInfo = new LibretroLibrary.retro_system_info();
        library.retro_get_system_info(systemInfo);

        System.out.println("-- START SYSTEM_INFO --");
        System.out.println("LIBRARY_NAME: " + systemInfo.library_name);
        System.out.println("LIBRARY_VERSION: " + systemInfo.library_version);
        System.out.println("VALID_EXTENSIONS: " + systemInfo.valid_extensions);
        System.out.println("NEED_FULLPATH: " + systemInfo.need_fullpath);
        System.out.println("BLOCK_EXTRACT: " + systemInfo.block_extract);
        System.out.println("-- END SYSTEM_INFO --");

        retroEnvironmentCallback = new LibretroLibrary.retro_environment_t() {
            @Override
            public byte apply(LibretroLibrary.UnsignedInt cmd, Pointer data) {
                return retroEnvironment(cmd, data);
            }
        };

        retroVideoRefreshCallback = new LibretroLibrary.retro_video_refresh_t() {
            @Override
            public void apply(Pointer data, LibretroLibrary.UnsignedInt width, LibretroLibrary.UnsignedInt height, LibretroLibrary.SizeT pitch) {
                retroVideoRefresh(data, width, height, pitch);
            }
        };

        retroAudioSampleCallback = new LibretroLibrary.retro_audio_sample_t() {
            @Override
            public void apply(short left, short right) {
                retroAudioSample(left, right);
            }
        };

        retroAudioSampleBatchCallback = new LibretroLibrary.retro_audio_sample_batch_t() {
            @Override
            public void apply(ShortByReference data, LibretroLibrary.SizeT frames) {
                retroAudioSampleBatch(data, frames);
            }
        };

        retroInputPollCallback = new LibretroLibrary.retro_input_poll_t() {
            @Override
            public void apply() {
                retroInputPoll();
            }
        };

        retroInputStateCallback = new LibretroLibrary.retro_input_state_t() {
            @Override
            public short apply(LibretroLibrary.UnsignedInt port, LibretroLibrary.UnsignedInt device, LibretroLibrary.UnsignedInt index, LibretroLibrary.UnsignedInt id1) {
                return retroInputState(port, device, index, id1);
            }
        };

        System.out.println("Setting environment callback...");
        library.retro_set_environment(retroEnvironmentCallback);

        System.out.println("Setting video refresh callback...");
        library.retro_set_video_refresh(retroVideoRefreshCallback);

        System.out.println("Setting audio sample callback...");
        library.retro_set_audio_sample(retroAudioSampleCallback);

        System.out.println("Setting audio sample batch callback...");
        library.retro_set_audio_sample_batch(retroAudioSampleBatchCallback);

        System.out.println("Setting input poll callback...");
        library.retro_set_input_poll(retroInputPollCallback);

        System.out.println("Setting input state callback...");
        library.retro_set_input_state(retroInputStateCallback);

        System.out.println("Initiating core...");
        library.retro_init();
    }

    public void deinit() {
        library.retro_deinit();

        library = null;
        System.gc();
    }

    public boolean loadGame(String path) {
        LibretroLibrary.retro_game_info gameInfo = loadGameInfo(path);

        boolean success = library.retro_load_game(gameInfo) != 0;

        if (!success) {
            System.out.println("ERROR: Libretro encountered an error while loading your game.");
            return false;
        }

        systemAvInfo = new LibretroLibrary.retro_system_av_info();
        library.retro_get_system_av_info(systemAvInfo);

        System.out.println("-- START SYSTEM_AV_INFO --");
        System.out.println("GEOMETRY:");
        System.out.println("BASE_WIDTH: " + systemAvInfo.geometry.base_width);
        System.out.println("BASE_HEIGHT: " + systemAvInfo.geometry.base_height);
        System.out.println("MAX_WIDTH: " + systemAvInfo.geometry.max_width);
        System.out.println("MAX_HEIGHT: " + systemAvInfo.geometry.max_height);
        System.out.println("ASPECT_RATIO: " + systemAvInfo.geometry.aspect_ratio);
        System.out.println("TIMING");
        System.out.println("FPS: " + systemAvInfo.timing.fps);
        System.out.println("SAMPLE_RATE: " + systemAvInfo.timing.sample_rate);
        System.out.println("-- END SYSTEM_AV_INFO --");

        int bufferSize = AudioTrack.getMinBufferSize((int) systemAvInfo.timing.sample_rate, AudioFormat.CHANNEL_OUT_STEREO, AudioFormat.ENCODING_PCM_16BIT);
        if (bufferSize > 0) {
            audio = new AudioTrack(AudioManager.STREAM_MUSIC, (int) systemAvInfo.timing.sample_rate, AudioFormat.CHANNEL_OUT_STEREO, AudioFormat.ENCODING_PCM_16BIT, bufferSize, AudioTrack.MODE_STREAM);
            audio.play();

            isAudioAvailable = true;
        }

        isPaused = false;

        return true;
    }

    public void unloadGame() {
        library.retro_unload_game();
    }

    public LibretroLibrary.retro_game_info loadGameInfo(String path) {
        LibretroLibrary.retro_game_info gameInfo = new LibretroLibrary.retro_game_info();

        File file = new File(path);
        int size = (int) file.length();
        byte[] bytes = new byte[size];

        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
            buf.read(bytes, 0, bytes.length);
            buf.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("GAME_INFO_PATH: " + path);
        System.out.println("GAME_INFO_SIZE: " + size);

        Pointer ptr = new Memory(bytes.length);
        ptr.write(0, bytes, 0, bytes.length);

        gameInfo.path = path;
        gameInfo.data = ptr;
        gameInfo.size = new LibretroLibrary.SizeT((long)size);

        return gameInfo;
    }

    private byte retroEnvironment(LibretroLibrary.UnsignedInt cmd, Pointer data) {
        LibretroLibrary.retro_environment environment = LibretroLibrary.retro_environment.get(cmd.intValue());

        if (environment == null) {
            // System.out.println("NULL environment: " + cmd.intValue());
            return 1;
        } else {
            // System.out.println("VALID environment: " + cmd.intValue());
        }

        switch (environment) {
            case RETRO_ENVIRONMENT_SET_PERFORMANCE_LEVEL:
                System.out.println("RETRO_ENVIRONMENT_SET_PERFORMANCE_LEVEL: " + data.getInt(0));
                return 1;
            case RETRO_ENVIRONMENT_GET_SYSTEM_DIRECTORY :
                File dir = new File(context.getFilesDir(), "system");
                dir.mkdirs();

                String path = dir.getAbsolutePath();
                System.out.println("RETRO_GET_SYSTEM_DIRECTORY: " + path);

                PointerByReference pointer = new PointerByReference(data);

                Pointer pathPointer = new Memory(path.length() + 1);
                pathPointer.setString(0, path);

                pointer.getValue().setPointer(0, pathPointer);

                return 1;
            case RETRO_ENVIRONMENT_SET_PIXEL_FORMAT:
                LibretroLibrary.retro_pixel_format format = LibretroLibrary.retro_pixel_format.get(data.getInt(0));

                switch (format) {
                    case RETRO_PIXEL_FORMAT_0RGB1555:
                        System.out.println("Pixel format not supported: RGB1555.");
                        break;
                    case RETRO_PIXEL_FORMAT_RGB565:
                        System.out.println("Changing pixel format to: RGB565.");
                        pixelFormat = PixelFormat.RGB_565;
                        pixelConfig = Bitmap.Config.RGB_565;
                        break;
                    case RETRO_PIXEL_FORMAT_XRGB8888:
                        System.out.println("Changing pixel format to: XRGB8888.");
                        pixelFormat = PixelFormat.RGBA_8888;
                        pixelConfig = Bitmap.Config.ARGB_8888;
                        break;
                    default:
                        break;
                }

                return 1;
            case RETRO_ENVIRONMENT_SET_INPUT_DESCRIPTORS:
                return 0;
            case RETRO_ENVIRONMENT_SET_VARIABLES:
                return 0;
            case RETRO_ENVIRONMENT_GET_VARIABLE:
                return 0;
            case RETRO_ENVIRONMENT_GET_VARIABLE_UPDATE:
                return 0;
            case RETRO_ENVIRONMENT_GET_RUMBLE_INTERFACE:
                return 0;
            case RETRO_ENVIRONMENT_GET_LOG_INTERFACE:
                return 0;
            case RETRO_ENVIRONMENT_GET_SAVE_DIRECTORY:
                File saveDir = new File(context.getFilesDir(), "save");
                saveDir.mkdirs();

                String savePath = saveDir.getAbsolutePath();
                System.out.println("RETRO_GET_SAVE_DIRECTORY: " + savePath);

                PointerByReference savePointer = new PointerByReference(data);

                Pointer savePathPointer = new Memory(savePath.length() + 1);
                savePathPointer.setString(0, savePath);

                savePointer.getValue().setPointer(0, savePathPointer);

                return 1;
            case RETRO_ENVIRONMENT_SET_SYSTEM_AV_INFO:
                return 0;
            case RETRO_ENVIRONMENT_SET_CONTROLLER_INFO:
                return 0;
            case RETRO_ENVIRONMENT_SET_GEOMETRY:
                return 1;
            case RETRO_ENVIRONMENT_SET_SERIALIZATION_QUIRKS:
                return 0;
            case RETRO_ENVIRONMENT_SET_MEMORY_MAPS:
                return 0;
            case RETRO_ENVIRONMENT_SET_SUPPORT_ACHIEVEMENTS:
                return 0;
            case RETRO_ENVIRONMENT_GET_AUDIO_VIDEO_ENABLE:
                return 0;
            default:
                return 0;
        }
    }

    public int pixelFormat;
    public Bitmap.Config pixelConfig;

    public ByteBuffer screenBuffer;
    public Bitmap screenBitmap;

    public int screenWidth;
    public int screenHeight;

    private void retroVideoRefresh(Pointer data, LibretroLibrary.UnsignedInt width, LibretroLibrary.UnsignedInt height, LibretroLibrary.SizeT pitch) {
        byte[] dataBuffer = new byte[height.intValue() * pitch.intValue()];

        PixelFormat pixelInfo = new PixelFormat();
        PixelFormat.getPixelFormatInfo(pixelFormat, pixelInfo);

        data.read(0, dataBuffer, 0, dataBuffer.length);

        byte[] screenBuffer = new byte[width.intValue() * height.intValue() * pixelInfo.bytesPerPixel];

        for (int i = 0; i < height.intValue(); i++) {
            System.arraycopy(dataBuffer, i * pitch.intValue(), screenBuffer, i * width.intValue() * pixelInfo.bytesPerPixel, width.intValue() * pixelInfo.bytesPerPixel);
        }

        if (pixelFormat == PixelFormat.RGBA_8888) {
            for (int i = 0; i < screenBuffer.length; i += pixelInfo.bytesPerPixel) {
                byte r = screenBuffer[i + 2];
                byte b = screenBuffer[i];
                screenBuffer[i] = r;
                screenBuffer[i + 2] = b;
                screenBuffer[i + 3] = (byte) 0xFF;
            }
        }

        screenWidth = width.intValue();
        screenHeight = height.intValue();

        this.screenBuffer = ByteBuffer.wrap(screenBuffer);
        this.screenBuffer.rewind();

        screenBitmap = Bitmap.createBitmap(width.intValue(), height.intValue(), pixelConfig);
        screenBitmap.copyPixelsFromBuffer(this.screenBuffer);
    }

    private void retroAudioSample(short left, short right) {
        if (!isAudioAvailable) return;
    }

    private void retroAudioSampleBatch(ShortByReference data, LibretroLibrary.SizeT frames) {
        if (!isAudioAvailable) return;

        Pointer pointer = data.getPointer();

        byte[] buffer = new byte[frames.intValue() * 4];
        pointer.read(0, buffer, 0, buffer.length);

        audio.write(buffer, 0, buffer.length);
    }

    private void retroInputPoll() {

    }

    private short retroInputState(LibretroLibrary.UnsignedInt port, LibretroLibrary.UnsignedInt device, LibretroLibrary.UnsignedInt index, LibretroLibrary.UnsignedInt id) {
        for (Map.Entry<Integer, Boolean> entry : keys.entrySet()) {
            if (entry.getKey() == id.intValue()) {
                return (short) (entry.getValue() ? 1 : 0);
            }
        }

        return 0;
    }

    public void saveState(File saveFile) {
        LibretroLibrary.SizeT size = library.retro_serialize_size();
        byte[] buffer = new byte[size.intValue()];

        Pointer ptr = new Memory(buffer.length);
        ptr.write(0, buffer, 0, buffer.length);

        library.retro_serialize(ptr, size);

        try {
            FileUtils.writeByteArrayToFile(saveFile, ptr.getByteArray(0, buffer.length));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadState(File saveFile) {
        try {
            byte[] buffer = FileUtils.readFileToByteArray(saveFile);

            System.out.println("BUFFER: " + buffer.length + ", " + saveFile.getAbsolutePath());

            Pointer ptr = new Memory(buffer.length);
            ptr.write(0, buffer, 0, buffer.length);

            library.retro_unserialize(ptr, new LibretroLibrary.SizeT(buffer.length));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void run() {
        library.retro_run();
    }
}

